//
//  FoundViewController.swift
//  SOPER-Techno1
//
//  Created by Timotius Leonardo Lianoto on 05/05/20.
//  Copyright © 2020 Technopreneur. All rights reserved.
//

import UIKit

class FoundViewController: UIViewController {

    @IBOutlet var fotoSupir: UIImageView!
    @IBOutlet var namaSupir: UILabel!
    @IBOutlet var kategoriSupir: UILabel!
    @IBOutlet var starSupir: UILabel!
    @IBOutlet var countTrips: UILabel!
    struct soper {
        let fotoSoper:String
        let namaSoper:String
        let kategoriSoper:String
        let starSoper:String
        let countTrips:Int
        
        static func fetchSoper() -> [soper] {
          return [
            soper(fotoSoper: "Felicia_AF", namaSoper: "Felicia Guns", kategoriSoper: "Not Valuable Soper", starSoper: "0.01", countTrips: 1),
            soper(fotoSoper: "Matu_AF", namaSoper: "Andre Manrines", kategoriSoper: "Valuable Soper", starSoper: "4.0", countTrips: 50),
            soper(fotoSoper: "Monica_AF", namaSoper: "Monica Adel", kategoriSoper: "Medium Soper", starSoper: "3.5", countTrips: 10),
            soper(fotoSoper: "Timol_AF", namaSoper: "Timotius Leonardo", kategoriSoper: "Valuable Soper", starSoper: "4.5", countTrips: 100),
            soper(fotoSoper: "Timotius_AF", namaSoper: "Timotius Tjahjadi", kategoriSoper: " Most Preffered Soper", starSoper: "5.0", countTrips: 999)
            ]
        }
    }
    var simpanDataSoper = soper.fetchSoper()
    override func viewDidLoad() {
        super.viewDidLoad()
        simpanDataSoper.shuffle()
        fotoSupir.image = UIImage(named: simpanDataSoper[0].fotoSoper)
        namaSupir.text = simpanDataSoper[0].namaSoper
        kategoriSupir.text = simpanDataSoper[0].kategoriSoper
        starSupir.text = simpanDataSoper[0].starSoper
        countTrips.text = String(simpanDataSoper[0].countTrips) + " Trips"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
