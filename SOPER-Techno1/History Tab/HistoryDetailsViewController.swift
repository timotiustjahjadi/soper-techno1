//
//  HistoryDetailsViewController.swift
//  SOPER-Techno1
//
//  Created by Monica Adelya on 05/05/20.
//  Copyright © 2020 Technopreneur. All rights reserved.
//

import UIKit

class HistoryDetailsViewController: UIViewController {

    @IBOutlet weak var tanggalpemesananlabel: UILabel!
    @IBOutlet weak var fotosupir: UIImageView!
    @IBOutlet weak var namaSupir: UILabel!
    @IBOutlet weak var alamatjemput: UILabel!
    @IBOutlet weak var alamattujuan: UILabel!
    @IBOutlet weak var nominalbayar: UILabel!
    @IBOutlet weak var totalbayar: UILabel!
    struct history {
        let tanggalpemesananlabel:String
        let fotosupir:String
        let namaSupir:String
        let alamatjemput:String
        let alamattujuan:String
        let nominalbayar:String
        let totalbayar:String
        
        static func fetchHistory() -> [history] {

            return [
            history(tanggalpemesananlabel: "March 21st 2020", fotosupir: "Monica_AF", namaSupir: "Monica Adel", alamatjemput: "Graha Family B-5", alamattujuan: "Galaxy Mall ", nominalbayar: "Rp 50.000", totalbayar: "Rp 50.000"),
            history(tanggalpemesananlabel: "March 28th 2020", fotosupir: "Profile", namaSupir: "Christopher Kevin S", alamatjemput: "Dian Istana C-7", alamattujuan: "Lenmarc Mall", nominalbayar: "Rp 50.000", totalbayar: "Rp 50.000"),
            history(tanggalpemesananlabel: "April 16th 2020", fotosupir: "Timol_AF", namaSupir: "Timotius Leonardo", alamatjemput: "Pakuwon City", alamattujuan: "Pasar Atum Mall", nominalbayar: "Rp 50.000", totalbayar: "Rp 50.000")

        ]
    }
}
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
