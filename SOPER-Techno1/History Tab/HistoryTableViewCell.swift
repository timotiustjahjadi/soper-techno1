//
//  HistoryTableViewCell.swift
//  SOPER-Techno1
//
//  Created by Timotius Leonardo Lianoto on 03/05/20.
//  Copyright © 2020 Technopreneur. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet var namaSupir : UILabel!
    @IBOutlet var tanggalPemesanan : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
