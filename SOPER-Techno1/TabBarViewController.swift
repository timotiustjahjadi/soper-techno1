//
//  TabBarViewController.swift
//  SOPER-Techno1
//
//  Created by Christopher Kevin Susandji on 30/04/20.
//  Copyright © 2020 Technopreneur. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let refreshAllTabs = Notification.Name("RefreshAllTabs")
}

class TabBarViewController: UITabBarController {

    let defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if defaults.bool(forKey: "First Launch") == false{

        }
        else {
            defaults.set(false, forKey: "First Launch")
            defaults.set(false, forKey: "Has Soper")
        }
        
        let currentViewControllers = self.viewControllers!
        let rideVC = currentViewControllers[0]
        let rideHasSoperVC = currentViewControllers[1]
        let historyVC = currentViewControllers[2]
        let profileVC = currentViewControllers[3]
        
        switch self.defaults.bool(forKey: "Has Soper") {
        case true:
            self.viewControllers = [rideHasSoperVC, historyVC, profileVC]
        
        case false:
            self.viewControllers = [rideVC, historyVC, profileVC]
        }

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(forName: .refreshAllTabs, object: nil, queue: nil) { (notification) in
            //check if its a normal user or comapny user
            
            switch self.defaults.bool(forKey: "Has Soper") {
            case true:
                self.viewControllers = [rideHasSoperVC, historyVC, profileVC]
            
            case false:
                self.viewControllers = [rideVC, historyVC, profileVC]
            }
            
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
